// Controle de Motores de Passo com joystick e Modulo driver A4988
// MODOS REMOTO E MANUAL

struct motor {
  String nome;
  String id;
  int pino_ena;                         // pino de ativacao (enable) A4988
  int pino_rst;                         // pino de reset do A4988
  int pino_step;                        // pino de passo (step) do A4988
  int pino_dir;                         // pino de direcao (direction) do A4988
  int sentido; 
  int botao_sentido_horario;
  int botao_sentido_antihorario;
};

// Botoes de acionamento
int botao_modo_manual = 2;              // VERDE               
int botao_cima = A1;                    // AMARELO
int botao_baixo = A2;                   // MARROM    (LILÁS)
int botao_direita = A3;                 // VERMELHO
int botao_esquerda = A4;                // LARANJA 
int modo_manual = false;

int meio_periodo = 700;                 // MeioPeriodo do pulso STEP em microsegundos F= 1/T = 1/1400 uS = 714,28 Hz
int PPS = 0;                            // Pulsos por segundo
 
motor motor_d;
motor motor_a;

void setup()
{
  motor_d.nome = "Motor Declinacao";
  motor_d.id = "D";
  motor_d.pino_dir = 3;                  // VERDE           
  motor_d.pino_step = 4;                 // AMARELO
  motor_d.pino_ena = 5;                  // LARANJA
  motor_d.pino_rst;                      // BRANCO
  motor_d.sentido = 'H';
  motor_d.botao_sentido_horario = botao_direita;
  motor_d.botao_sentido_antihorario = botao_esquerda;
  
  motor_a.nome = "Motor Ascensao Reta";
  motor_a.id = "A";
  motor_a.pino_dir = 6;                  // CINZA
  motor_a.pino_step = 7;                 // ROXO
  motor_a.pino_ena = 8;                  // AZUL
  motor_a.pino_rst;                      // BRANCO
  motor_a.sentido = 'H';
  motor_a.botao_sentido_horario = botao_cima;
  motor_a.botao_sentido_antihorario = botao_baixo;
  
  Serial.begin(9600);
  // Serial.setTimeout(500);

  // Configura pinos de controle do driver como saida
  pinMode(motor_d.pino_dir,  OUTPUT); 
  pinMode(motor_d.pino_step, OUTPUT); 
  pinMode(motor_d.pino_ena,  OUTPUT); 
  pinMode(motor_a.pino_dir,  OUTPUT); 
  pinMode(motor_a.pino_step, OUTPUT); 
  pinMode(motor_a.pino_ena,  OUTPUT); 

  // Definindo pinos dos botoes como entrada PULLUP
  pinMode(botao_modo_manual, INPUT_PULLUP);  
  pinMode(botao_cima,        INPUT_PULLUP);
  pinMode(botao_baixo,       INPUT_PULLUP);
  pinMode(botao_esquerda,    INPUT_PULLUP);
  pinMode(botao_direita,     INPUT_PULLUP); 

  attachInterrupt(digitalPinToInterrupt(botao_modo_manual), interrupcaoModoManual, CHANGE);

  configuraMotor(motor_d);
  configuraMotor(motor_a);
}

void configuraMotor(motor &motor_x) 
{
  Serial.println("# " + motor_x.nome + " - Configuracoes iniciais");
  desativaChipA4988(motor_x);
  resetaChipA4988(motor_x);
  ativaChipA4988(motor_x);
  defineSentido(motor_x, 'A');
  desativaChipA4988(motor_x);
}

void resetaChipA4988(motor &motor_x)
{
  digitalWrite(motor_x.pino_rst, LOW);               // Realiza o reset do A4988
  delay (100);                                       // Atraso de 10 milisegundos
  digitalWrite(motor_x.pino_rst, HIGH);              // Libera o reset do A4988
  delay (100);                                       // Atraso de 10 milisegundos
  Serial.println("# " + motor_x.nome + " - Reset do driver");
}

void desativaChipA4988(motor &motor_x)
{
  digitalWrite(motor_x.pino_ena, HIGH);              // Desativa o chip A4988
  Serial.println("# " + motor_x.nome + " - Driver desativado");
  delay (100);                                       // Atraso de 10 milisegundos
}

void ativaChipA4988(motor &motor_x)
{
  digitalWrite(motor_x.pino_ena, LOW);               // Ativa o chip A4988
  Serial.println("# " + motor_x.nome + " - Driver ativado");
  delay (100);                                       // Atraso de 10 milisegundos
}

bool pressionado(int botao_x) {

  return digitalRead(botao_x) == LOW;
}

void defineSentido(motor &motor_x, char letra_sentido)    // Configura o sentido de rotacao do Motor
{   
  if(motor_x.sentido == 'A' && letra_sentido == 'H')
  {
    Serial.println("# " + motor_x.nome + " - Sentido Horario");
    digitalWrite(motor_x.pino_dir, HIGH);            // Configura o sentido HORARIO
  }
  else if (motor_x.sentido == 'H' && letra_sentido == 'A')
  { 
    Serial.println("# " + motor_x.nome + " - Sentido Anti-horario");
    digitalWrite(motor_x.pino_dir, LOW);             // Configura o sentido ANTI-HORARIO
  }

  motor_x.sentido = letra_sentido;
}

void giraUmPasso(motor &motor_x)                     // Pulso do passo do Motor
{
  digitalWrite(motor_x.pino_step, LOW);              // Pulso nivel baixo
  delayMicroseconds (meio_periodo);                  // meio_periodo de X microsegundos
  digitalWrite(motor_x.pino_step, HIGH);             // Pulso nivel alto
  delayMicroseconds (meio_periodo);                  // meio_periodo de X microsegundos
}

void giraMotor(motor &motor_x, char letra_sentido, int passos)
{
  defineSentido(motor_x, letra_sentido);

  int passos_realizados = 0;

  for (int i = 0; i < passos; i++)                   // Avanca N passos no Motor
  {

    giraUmPasso(motor_x);                             
    passos_realizados++;
    if (modo_manual) 
    {
      break;
    }
  }
  if (!modo_manual) 
  {
  Serial.println (motor_x.id + "," + letra_sentido + "," + passos_realizados); // Informa qnt de passos realizados.
  }
}

void modoManual() 
{
  Serial.println("# Modo manual HABILITADO");
  ativaChipA4988(motor_d);
  ativaChipA4988(motor_a);

  while(modo_manual) 
  {
    if(pressionado(motor_d.botao_sentido_horario))
    {
      giraMotor(motor_d, 'H', 1);
    }
    else if(pressionado(motor_d.botao_sentido_antihorario))
    {
      giraMotor(motor_d, 'A', 1);
    }

    if(pressionado(motor_a.botao_sentido_horario))
    {
      giraMotor(motor_a, 'H', 1);
    }
    else if(pressionado(motor_a.botao_sentido_antihorario))
    {
      giraMotor(motor_a, 'A', 1);
    }
  }

  desativaChipA4988(motor_d);
  desativaChipA4988(motor_a);
  Serial.println("# Modo manual DESABILITADO");
}

void modoRemoto() 
{
  // dados a serem recebidos no formato MOTOR,SENTIDO,PASSOS
  // exemplos: "D,H,9999" e "A,A,9999"
  String recebido = Serial.readStringUntil('\n');
  Serial.println("# recebido: " + recebido);

  char letra_motor = recebido.charAt(0);       // pega primeiro caracter recebido
  char letra_sentido = recebido.charAt(2);     // pega terceiro caracter recebido
  int passos = recebido.substring(4).toInt();  // pega do quinto caracter recebido em diante como inteiro

  motor motor_x;

  if (letra_motor == 'D')
  {
    ativaChipA4988(motor_d);
    giraMotor(motor_d, letra_sentido, passos);
    desativaChipA4988(motor_d);
  }
  else if (letra_motor == 'A')
  {
    ativaChipA4988(motor_a);
    giraMotor(motor_a, letra_sentido, passos);
    desativaChipA4988(motor_a);
  }
}

void loop()
{  
  if(modo_manual)       // Se botao modo manual estiver pressionado
  {
    modoManual();
  }
  else if(Serial.available() > 0)          // Caso contrario, se houver dados para ler
  {
    modoRemoto();
  }

  delay(500);
}

void interrupcaoModoManual()
{
  if(pressionado(botao_modo_manual))       // Se botao modo manual estiver pressionado
  {
    modo_manual = true;
  } else {
    modo_manual = false;
  }
}