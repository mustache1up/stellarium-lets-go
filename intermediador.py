#!/bin/python3
import time, urllib.request, json, re,sys, serial 

# constantes:
passos_ha_por_90_graus  = 6800
passos_ha_por_hora      = passos_ha_por_90_graus / 6
passos_ha_por_minuto    = passos_ha_por_hora / 60
passos_ha_por_segundo   = passos_ha_por_minuto / 60

passos_dec_por_90_graus = 6800
passos_dec_por_grau     = passos_dec_por_90_graus / 90
passos_dec_por_minuto   = passos_dec_por_grau / 60
passos_dec_por_segundo  = passos_dec_por_minuto / 60

_24_horas_em_segundos = (24 * 60 * 60)
_12_horas_em_segundos = _24_horas_em_segundos / 2

# inicializa variaveis
ha_telescopio_segundos = 0
dec_telescopio_segundos = 0
primeira_vez = True

# inicia comunicacao com o arduino
arduino = serial.Serial('com6', 9600, timeout=0.1)

while True:

  # inicializa variaveis do loop:
  aguardar_resposta_motor_a = False
  aguardar_resposta_motor_d = False

  # 1. pegar HA e DEC via intermediador:
  
  # captura as coordenadas do objeto selecionado no stellarium via remote control plugin
  dados = json.loads(urllib.request.urlopen('http://localhost:8090/api/main/status', timeout=2).read())

  if not dados['selectioninfo']:
    print(f'Nenhum objeto selecionado')

  else:
    m = re.search('<h2>(.*?)(\s.*)?</h2>', dados['selectioninfo'])
    nome_do_objeto = m.group(1)
    try:
      print(f'Astro atual:')
      print(f'  {nome_do_objeto}')
      print()
    except:
      pass
      
    # interpreta os dados recebidos, exemplo: ... HA/Dec:   16h42m50.12s/-69°49'37.7\"   (apparent) ...
    m = re.search('(HA/Dec|AH/DEC): *((\d+)h(\d+)m(\d+\.\d+)s)/(([-+]\d+)°(-?\d+)\'(\d+\.\d+)\")', dados['selectioninfo'])

    # _novo_ são os dados que acabaram de ser lidos do stellarium
    ha_novo_horas = int(m.group(3))
    ha_novo_minutos = int(m.group(4))
    ha_novo_segundos = float(m.group(5)) # é um numero com decimal

    ha_novo_segundos = ha_novo_segundos + (ha_novo_minutos * 60) + (ha_novo_horas * 60 * 60)

    dec_novo_graus = int(m.group(7))
    dec_novo_minutos = int(m.group(8))
    dec_novo_segundos = float(m.group(9)) # é um numero com decimal

    dec_novo_segundos = dec_novo_segundos + (dec_novo_minutos * 60) + (dec_novo_graus * 60 * 60)

    print(f'Posicao atual do astro:')
    print(f'  HA em segundos: {ha_novo_segundos}s')
    print(f'  Dec em segundos: {dec_novo_segundos}s"')
    print()

    # 2. guardar essas posicoes

    # esse bloco só é executado uma vez, ao iniciar esse script
    if primeira_vez:
      ha_telescopio_segundos = ha_novo_segundos
      dec_telescopio_segundos = dec_novo_segundos
      primeira_vez = False

    print(f'Posicao atual telescopio:')
    print(f'  HA em segundos: {ha_telescopio_segundos}s')
    print(f'  Dec em segundos: {dec_telescopio_segundos}s"')
    print()

    # 3.1 obter a diferença desde a ultima movimentação

    ha_diferenca_segundos  = round(ha_novo_segundos  - ha_telescopio_segundos, 2)
    dec_diferenca_segundos = round(dec_novo_segundos - dec_telescopio_segundos, 2)

    print(f'Diferenca astro - telescopio:')
    print(f'  HA em segundos: {ha_diferenca_segundos}s')
    print(f'  Dec em segundos: {dec_diferenca_segundos}s"')
    print()

    # 3.2 converter HA e Dec para passos
    
    total_de_passos_pra_mover_no_motor_ha  = ha_diferenca_segundos * passos_ha_por_segundo
    total_de_passos_pra_mover_no_motor_ha  = int(round(total_de_passos_pra_mover_no_motor_ha, 0))

    total_de_passos_pra_mover_no_motor_dec = dec_diferenca_segundos * passos_dec_por_segundo
    total_de_passos_pra_mover_no_motor_dec = int(round(total_de_passos_pra_mover_no_motor_dec, 0))
    
    if total_de_passos_pra_mover_no_motor_ha >= 0:
      sentido_motor_ha = 'H'
    else:
      sentido_motor_ha = 'A'
        
    if total_de_passos_pra_mover_no_motor_dec <= 0:
      sentido_motor_dec = 'H'
    else:
      sentido_motor_dec = 'A'
    
    print(f'Quantidade passos:')
    print(f'  Motor A: {total_de_passos_pra_mover_no_motor_ha}')
    print(f'  Motor D: {total_de_passos_pra_mover_no_motor_dec}')
    print()

    # 4. mandar info pro arduino

    comando_motor_ha  = f'A,{sentido_motor_ha},{abs(total_de_passos_pra_mover_no_motor_ha)}\n'
    comando_motor_dec = f'D,{sentido_motor_dec},{abs(total_de_passos_pra_mover_no_motor_dec)}\n'

    print(f'Comandos para os motores:')
    print(f'  {comando_motor_ha}')
    print(f'  {comando_motor_dec}')

    # efetivamente manda os comandos para o arduino:
    if total_de_passos_pra_mover_no_motor_ha != 0:
      arduino.write(bytes(comando_motor_ha, 'utf-8'))
      aguardar_resposta_motor_a = True
    
    if total_de_passos_pra_mover_no_motor_dec != 0:
      arduino.write(bytes(comando_motor_dec, 'utf-8'))
      aguardar_resposta_motor_d = True

    sys.stdout.flush()
    
  print(f'\n\n\n --- \n\n\n')  

  print(f'Resposta do arduino:')  

  while aguardar_resposta_motor_a or aguardar_resposta_motor_d:

    msg = arduino.readline()

    if not msg.strip():
      continue

    msg_tratada = str(msg).replace("b'", "").replace("'", "").replace("\\r\\n", "").rstrip()
    print(f'  {msg_tratada}')
    sys.stdout.flush()
    if not msg_tratada.startswith("#") and msg_tratada != "":

      # converter passos para HA ou Dec (de acordo com a letra do motor)

      letra_motor, letra_sentido, passos_realizados = msg_tratada.split(",")

      if letra_motor == 'A':
        
        segundos_ha_realizados = float(passos_realizados) / passos_ha_por_segundo
        
        if letra_sentido == 'H':
          ha_telescopio_segundos = ha_telescopio_segundos + segundos_ha_realizados
        else:
          ha_telescopio_segundos = ha_telescopio_segundos - segundos_ha_realizados

        aguardar_resposta_motor_a = False

      elif letra_motor == 'D':

        segundos_dec_realizados = float (passos_realizados) / passos_dec_por_segundo
            
        if letra_sentido == 'H':
          dec_telescopio_segundos = dec_telescopio_segundos - segundos_dec_realizados
        else:
          dec_telescopio_segundos = dec_telescopio_segundos + segundos_dec_realizados

        aguardar_resposta_motor_d = False

      print(f'  Quantidade realizados pelo arduino no motor {letra_motor}: {passos_realizados}')
      sys.stdout.flush()
      
    time.sleep(0.1)
  time.sleep(2)
  print(f'\n\n\n --- \n\n\n')