# Instruções:

## Para executar o intermediador:

- abrir o stellarium

- habilitar o plugin de controle remoto

- abrir o Git Bash dentro do diretório onde está o arquivo `intermediador.py`

- rodar o comando abaixo:

```bash
python3 intermediador.py
```